function twoDimensional(arr, number) {
    const newArray = [];
    let carret = -1;
    for (let i = 0; i < arr.length; i++) {
        if (i % number === 0) {
            carret++;
            newArray[carret] = [];
        }
        newArray[carret].push(arr[i]);
    }
    return newArray;
}
console.log(twoDimensional([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 4));


function findLargest() {
    const newArr = [];
    for (let i = 0; i < arguments.length; i++) {
            newArr.push(Math.max.apply(null, arguments[i]));
    }
    return newArr;
}
console.log(findLargest([1,2,4], [2,4,3], [43,2,1]));


function findEl(arr, callback) {
    const newArr = [];

    for (let i = 0; i < arr.length; i++) {
        if(callback(arr[i])) {
            newArr.push(arr[i]);
        }
    }
    return newArr;
}
console.log(findEl([1, 5, 10, 19, 23, 30], function(num) { return num % 2 === 0; }))


function letterAlphabet(string) {
    let str = string.split('').sort().join('');
    const newArr = [];
    for (let i = 0; i < str.length - 1; i++) {
        if (str.charCodeAt(i + 1) - str.charCodeAt(i) != 1) {
            newArr.push(String.fromCharCode(str.charCodeAt(i) + 1));
        }
    }
    console.log(newArr);
}
letterAlphabet("bag")


function fearNotLetter(str) {
    var allChars = '';
    var notChars = new RegExp('[^'+str+']','g');

    for (var i = 0; allChars[allChars.length-1] !== str[str.length-1] ; i++)
        allChars += String.fromCharCode(str[0].charCodeAt(0) + i);

    return allChars.match(notChars) ? allChars.match(notChars).join('') : undefined;
}

// test here
fearNotLetter("abce");


